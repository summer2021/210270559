import urllib3
from bs4 import BeautifulSoup
import re
import pandas as pd
http=urllib3.PoolManager()
gstatic={}
gg=[]
def stepdic(n,d,v):
    if d<=100:
        gstatic[n][(d//10)]+=int(v)
def static(n,aa):
    try:
        gstatic[n]
    except:
        gstatic[n]=[0]*11
    for a in aa:
        dd=eval(a)
        stepdic(n,dd['score'],dd['count'])
for i in range(1,680):
    show={}
    url=f'https://uoj.ac/problem/{i}/statistics'
    r = http.request('GET', url)
    if r.status==200:
        print(f'--process {i}--')
        html=r.data.decode('utf-8')
        soup = BeautifulSoup(html,'html.parser',from_encoding='utf-8')
        name=soup.find_all('title')[0].text
        name=name[:name.find(' -')]
        for t in soup.find_all('script',type="text/javascript"):
            s=t.extract().string
            if s and ('score-distribution-chart\'' in s):static(i,re.findall(r'({.*?})', s))
        show['curl']=f'https://uoj.ac/problem/{i}'
        show['name']=name
        for j in range(9):
            show[f'[{j*10},{(j+1)*10})']=gstatic[i][j]
        show[f'(90,100]']=gstatic[i][9]+gstatic[i][10]
        gg.append(show)
    else:
        print(f'--fail {i}--')
dataframe = pd.DataFrame(gg)
dataframe.to_csv("data",index=False,sep=',')