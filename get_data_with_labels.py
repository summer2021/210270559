from urllib.request import urlopen
import urllib3
from bs4 import BeautifulSoup
import re
import time
import pandas as pd
http=urllib3.PoolManager()
from selenium import webdriver
from pyquery import PyQuery as pq


# 读取数据
data = pd.read_csv("data0.csv")
browser = webdriver.Chrome()

fail = []
for i in range(640):
    if i % 10 == 0:
        data.to_csv("data.csv")
    try:
        url = 'https://www.luogu.com.cn/problem/list?keyword='+data.iloc[i, 1]+'&page=1'
        browser.get(url)
        # browser.find_element_by_class_name('lfe-form-sz-middle').send_keys(data.iloc[i, 1])
        # browser.find_element_by_xpath('/html/body/div[1]/div[2]/main/div/section/div/section[1]/div[1]/div/button').click()
        # time.sleep(2)
        browser.find_element_by_xpath('//*[@id="app"]/div[2]/main/div/div/div/div[1]/div[1]/div/div[4]/span/a').click()
        time.sleep(1)
        html=browser.page_source
        soup = BeautifulSoup(html, 'html.parser',from_encoding='utf-8')
        k = soup.find_all('div', class_="tags-wrap multiline")[0]
        labels = []
        if 'lfe-caption' not in str(k):
            print(f'----- fail{i} -----')
            fail.append(i+1)
        else:
            for t in k.find_all('span',class_='lfe-caption'):
                label = re.findall(r'(?:>).*?<', str(t))[0].strip('<>')
                labels.append(label)
            result = ' '.join(labels)
            data.loc[i,'labels'] = result
            print(f'----- success{i} -----')
    except:
        print(f'没有找到{i}')
        fail.append(i + 1)
data.to_csv("data.csv")
print(len(fail))
print(fail)



# for i in range(640):
#     show={}
#     url=f'https://uoj.ac/problem/{i}/statistics'
#     r = http.request('GET', url)
#     if r.status==200:
#         print(f'--process {i}--')
#         html=r.data.decode('utf-8')
#         soup = BeautifulSoup(html,'html.parser',from_encoding='utf-8')
#         name=soup.find_all('title')[0].text
#         name=name[:name.find(' -')]
#         for t in soup.find_all('script',type="text/javascript"):
#             s=t.extract().string
#             if s and ('score-distribution-chart\'' in s):static(i,re.findall(r'({.*?})', s))
#         show['curl']=f'https://uoj.ac/problem/{i}'
#         show['name']=name
#         for j in range(9):
#             show[f'[{j*10},{(j+1)*10})']=gstatic[i][j]
#         show[f'(90,100]']=gstatic[i][9]+gstatic[i][10]
#         gg.append(show)
#     else:
#         print(f'--fail {i}--')
