##  判断用户

- 新用户：New_user.ipynb  推荐模板题
- 老用户：LFM.ipynb
- ![flowchart](C:\Users\DELL\Desktop\终期报告\flowchart.png) 





##  其他文件

- all_user.csv: 爬取了445个用户的做题数据。每个人都至少做过五道题。
- data_label_encode.csv:所有题目的信息，包括网址，题目序号，标签，ac率，属于十四种题型中的哪几类等
- data_label_encode.ipynb:根据题目信息里的标签 加上属于十四类中题型的那几类
- ifm.model：通过LFM模型保存下来的P,Q的值